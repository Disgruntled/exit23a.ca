# My personal website

This is the repo of my personal website, it exists here as part of my gitops/CICD workflow for updating content.

The gitlab runner on receipt of a new commit to master will deploy this website to cloudflare workers.

The live website can be viewed @ https://exit23.ca